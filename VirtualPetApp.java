import java.util.Scanner;
public class VirtualPetApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Panda[] embarrassment = new Panda[1];
		
		//for loop to get values for the constructor
		for (int i = 0; i < embarrassment.length; i++) {
			System.out.println("What is the name of the " + (i+1) + "st panda: ");
			String name = reader.nextLine();
			System.out.println("How much does " + name + " weight (in kg): ");
			double weight = Double.parseDouble(reader.nextLine());
			System.out.println("How many hours does " + name + " sleep in average in a day: ");
			int hoursSleep = Integer.parseInt(reader.nextLine());
			embarrassment[i] = new Panda(name, weight, hoursSleep);
			
		}
		System.out.println(""); //Line Break
		
		//Print all the fields of the last animal
		System.out.println("Name : " + embarrassment[embarrassment.length-1].getName());
		System.out.println("Weight : " + embarrassment[embarrassment.length-1].getWeight());
		System.out.println("Hours Slept : " +  embarrassment[embarrassment.length-1].getHoursSleep());

		System.out.println("Update " + embarrassment[embarrassment.length-1].getName() + "'s weight (in kg): ");
		embarrassment[embarrassment.length-1].setWeight(Double.parseDouble(reader.nextLine()));
		
		System.out.println(""); //Line Break
		
		//Print all the fields of the last animal with its updated weight
		System.out.println("Name : " + embarrassment[embarrassment.length-1].getName());
		System.out.println("Updated Weight : " + embarrassment[embarrassment.length-1].getWeight());
		System.out.println("Hours Slept : " + embarrassment[embarrassment.length-1].getHoursSleep());

	}
}
