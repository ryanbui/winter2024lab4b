public class Panda {
	private String name;
	private double weight;
	private int hoursSleep;
	
	//constructor
	public Panda(String name, double weight, int hoursSleep){
		this.name = name;
		this.weight = weight;
		this.hoursSleep = hoursSleep;
	}
	
	//setter methods
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	//getter methods
	public String getName(){
		return this.name;
	}
	public double getWeight(){
		return this.weight;
	}
	public int getHoursSleep(){
		return this.hoursSleep;
	}
	
	public void eatBamboos() {
		if (this.weight >= 120) {
			System.out.println(name + " you are overweight! You should eat less bamboos!");
		} else {
			System.out.println(name + " you are underweight! Please eat more bamboos!"); 
		}
	}
	
	public void sleep() {
		if (this.hoursSleep >= 10) {
			System.out.println(name + " you are sleeping too much! Go do something productive!");
		} else {
			System.out.println(name + " you are not sleeping enough! Pandas need around 10 hours of sleep per day!");
		}
	}
}
